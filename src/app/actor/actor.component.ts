import { Component, OnInit } from '@angular/core';
import { DatabaseService } from '../database.service';

@Component({
  selector: 'app-actor',
  templateUrl: './actor.component.html',
  styleUrls: ['./actor.component.css']
})
export class ActorComponent implements OnInit {
  actorsDB: any[] = [];
  movieDB: any[] = [];
  section = 1;
  fullName = '';
  bYear = 0;
  actorId = '';
  movieId = '';
  movieTitle = '';
  movieYear = 0;
  aYear = 0;
  movieRange: any[] = [];

  constructor(private dbService: DatabaseService) {}

  ngOnInit() {
    this.getActors();
    this.getMovies();
  }

  addMovie() {
    const obj = { title: this.movieTitle, year: this.movieYear, actors: [] };
    this.dbService.createMovie(obj).subscribe(result => {
      this.changeSection(1);
      this.getMovies();
    });
  }

  getMovies() {
    this.dbService.getMovies().subscribe((data: any[]) => {
      this.movieDB = data;
    });
  }

  addActorToMovie() {
    const obj = { movieID: this.movieId, actorID: this.actorId };
    this.dbService.addActor(obj).subscribe(result => {
      this.changeSection(1);
      this.getActors();
      this.getMovies();
    });
  }

  deleteMovie(id) {
    this.dbService.deleteMovie(id).subscribe(result => {
      this.getMovies();
      this.getActors();
    });
  }

  deleteMovieYearRange() {
    this.dbService.getMovieRange(this.aYear).subscribe((data: any[]) => {
      this.movieRange = data;
      this.movieRange.forEach(item => {
        this.dbService.deleteMovie(item._id).subscribe(result => {
          this.changeSection(1);
          this.getMovies();
          this.getActors();
        });
      });
    });
  }

  changeSection(sectionId) {
    this.section = sectionId;
    this.resetProperties();
  }

  resetProperties() {
    this.fullName = '';
    this.bYear = 0;
    this.actorId = '';
    this.movieId = '';
    this.movieTitle = '';
    this.movieYear = 0;
    this.aYear = 0;
    this.movieRange = [];
  }

  getActors() {
    this.dbService.getActors().subscribe((data: any[]) => {
      this.actorsDB = data;
    });
  }

  saveActor() {
    const obj = { name: this.fullName, bYear: this.bYear };
    this.dbService.createActor(obj).subscribe(result => {
      this.changeSection(1);
      this.getActors();
      this.getMovies();
    });
  }

  onSelectUpdate(item) {
    this.fullName = item.name;
    this.bYear = item.bYear;
    this.actorId = item._id;
  }

  updateActor() {
    const obj = { name: this.fullName, bYear: this.bYear };
    this.dbService.updateActor(this.actorId, obj).subscribe(result => {
      this.getActors();
      this.getMovies();
    });
  }

  deleteActor(item) {
    this.dbService.deleteActor(item._id).subscribe(result => {
      this.getActors();
      this.getMovies();
    });
  }
}
