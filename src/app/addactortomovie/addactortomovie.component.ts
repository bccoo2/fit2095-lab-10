import { Component, OnInit } from '@angular/core';
import { DatabaseService } from '../database.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-addactortomovie',
  templateUrl: './addactortomovie.component.html',
  styleUrls: ['./addactortomovie.component.css']
})
export class AddactortomovieComponent implements OnInit {
  actorsDB: any[] = [];
  movieDB: any[] = [];
  actorId = '';
  movieId = '';

  constructor(private dbService: DatabaseService, private router: Router) {}

  ngOnInit() {
    this.onGetActors();
    this.onGetMovies();
  }

  addActorToMovie() {
    const obj = { movieID: this.movieId, actorID: this.actorId };
    this.dbService.addActor(obj).subscribe(result => {
      this.router.navigate(['/listmovies']);
    });
  }

  onGetMovies() {
    this.dbService.getMovies().subscribe((data: any[]) => {
      this.movieDB = data;
    });
  }

  onGetActors() {
    this.dbService.getActors().subscribe((data: any[]) => {
      this.actorsDB = data;
    });
  }
}
